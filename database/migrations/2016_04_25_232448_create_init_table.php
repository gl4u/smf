<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->increments('user_id')->unsigned();
            $table->string('status', 255)->nullable();
            $table->string('admin_level', 16)->nullable();
            $table->dateTime('created_at');

            $table->engine = 'InnoDB';
        });

        Schema::create('user_sns_auths', function(Blueprint $table) {
            $table->increments('user_auth_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->string('provider', 255)->nullable();
            $table->string('provider_user_uid', 255)->nullable();
            $table->string('provider_token', 255)->nullable();
            $table->string('provider_acount_name', 255)->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->dateTime('created_at');
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('user_id')->references('user_id')->on('users');
            $table->engine = 'InnoDB';
        });
        \Schema::create('user_email_auths', function ($table) {
            $table->increments('user_email_auth_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->string('user_email', 255)->nullable();
            $table->string('user_password', 255)->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->integer('verified')->default(0);
            $table->dateTime('created_at');
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('user_id')->references('user_id')->on('users');
            $table->engine = 'InnoDB';
        });


        Schema::create('user_sessions', function(Blueprint $table) {
            $table->increments('user_session_id');
            $table->integer('user_id')->unsigned();

            $table->string('session_uid', 32);
            $table->string('session_token', 255);

            $table->string('os_type', 255);
            $table->string('os_version', 255);
            $table->string('app_version', 255);
            $table->string('device_model', 255);

            $table->dateTime('created_at');
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('user_id')->references('user_id')->on('users');
            $table->engine = 'InnoDB';
        });

        Schema::create('customers', function(Blueprint $table) {
            $table->increments('customer_id');
            $table->integer('user_id')->unsigned();
            $table->string('email', 255)->nullable();
            $table->string('display_name', 255)->nullable();
            $table->string('picture_name', 255)->nullable();
            $table->date('birth')->nullable();

            $table->string('region_code', 3)->nullable();
            $table->string('country_code', 3)->nullable();
            $table->string('division_code', 255)->nullable();

            $table->foreign('user_id')
                ->references('user_id')->on('users');

            $table->engine = 'InnoDB';
        });

        Schema::create('customer_media', function(Blueprint $table) {
            $table->increments('customer_media_id');
            $table->integer('customer_id')->unsigned();
            $table->string('media_theme', 255)->nullable();
            $table->string('customer_media_code', 255)->nullable();
            $table->string('media_kind', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('url', 255)->nullable();

            $table->foreign('customer_id')->references('customer_id')->on('customers');
            $table->engine = 'InnoDB';

        });
        Schema::create('clients', function(Blueprint $table) {
            $table->increments('client_id');
            $table->integer('user_id')->unsigned();
            $table->string('client_code', 255)->nullable();
            $table->string('client_name', 255)->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('user_id')->references('user_id')->on('users');
            $table->engine = 'InnoDB';
        });

        Schema::create('client_banks', function(Blueprint $table) {
            $table->increments('client_bank_id');
            $table->integer('client_id')->unsigned();
            $table->string('swiftcode', 255)->nullable();
            $table->string('branch_name', 255)->nullable();
            $table->string('branch_code', 255)->nullable();
            $table->string('beneficiary_name', 255)->nullable();
            $table->string('account', 255)->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('client_id')->references('client_id')->on('clients');
            $table->engine = 'InnoDB';
        });

        Schema::create('shops', function(Blueprint $table) {
            $table->increments('shop_id');
            $table->integer('client_id')->unsigned();
            $table->string('shop_name', 255)->nullable();
            $table->string('shop_address', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('client_id')->references('client_id')->on('clients');
            $table->engine = 'InnoDB';
        });

        Schema::create('campaigns', function(Blueprint $table) {
            $table->increments('campaign_id');
            $table->integer('client_id')->unsigned();
            $table->integer('shop_id')->nullable()->unsigned();
            $table->string('campaign_code', 255)->nullable();
            $table->string('campaign_name', 255)->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('currency_code', 255)->nullable();
            $table->string('reward_kind', 255)->nullable();
            $table->decimal('reward_rate', 9,2)->nullable();
            $table->string('redirect_url', 255)->nullable();
            $table->string('image_name', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('client_id')->references('client_id')->on('clients');
            $table->foreign('shop_id')->references('shop_id')->on('shops');
            $table->engine = 'InnoDB';
        });
        Schema::create('campaign_histories', function(Blueprint $table) {
            $table->increments('campaign_history_id');
            $table->integer('campaign_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->tinyInteger('approval_status')->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('campaign_id')->references('campaign_id')->on('campaigns');
            $table->foreign('customer_id')->references('customer_id')->on('customers');
            $table->engine = 'InnoDB';
        });
        Schema::create('coupons', function(Blueprint $table) {
            $table->increments('coupon_id');
            $table->integer('campaign_id')->unsigned();
            $table->string('coupon_name', 255)->nullable();
            $table->dateTime('publish_date')->nullable();
            $table->dateTime('expire_date')->nullable();
            $table->string('kind', 255)->nullable();
            $table->integer('distribution_number')->nullable();
            $table->string('currency_code', 255)->nullable();
            $table->decimal('discount_value', 10,2)->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->string('image', 255)->nullable();
            $table->string('status', 255)->nullable();

            $table->dateTime('valid_period_date')->nullable();
            $table->integer('valid_period_hours')->nullable();

            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('campaign_id')->references('campaign_id')->on('campaigns');
            $table->engine = 'InnoDB';
        });
        Schema::create('coupon_histories', function(Blueprint $table) {
            $table->increments('coupon_history_id');
            $table->integer('customer_id')->unsigned();
            $table->integer('coupon_id')->unsigned();
            $table->integer('expression_value')->nullable();
            $table->binary('photo')->nullable();
            $table->dateTime('expired_date')->nullable();
            $table->tinyInteger('activate')->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('customer_id')->references('customer_id')->on('customers');
            $table->foreign('coupon_id')->references('coupon_id')->on('coupons');
            $table->engine = 'InnoDB';
        });
        Schema::create('smile_ranks', function(Blueprint $table) {
            $table->increments('smile_rank_id');
            $table->integer('client_id')->unsigned();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('client_id')->references('client_id')->on('clients');
            $table->engine = 'InnoDB';
        });

        Schema::create('smile_rank_pictures', function(Blueprint $table) {
            $table->increments('smile_picture_id');
            $table->integer('smile_rank_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->binary('picture_uid')->nullable();

            $table->string('provider_picture_uid', 255)->nullable();
            $table->string('file_name', 255)->nullable();
            $table->string('total_like', 255)->nullable();
            $table->string('comment', 255)->nullable();
            $table->string('tags', 255)->nullable();
            $table->integer('smile_level');

            $table->dateTime('created_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('user_id')->references('user_id')->on('users');
            $table->engine = 'InnoDB';
        });

        Schema::create('conversions', function(Blueprint $table) {
            $table->increments('conversion_id');
            $table->integer('campaign_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('customer_media_id')->unsigned()->nullable();
            $table->integer('coupon_id')->unsigned()->nullable();
            $table->integer('coupon_history_id')->unsigned()->nullable();
            $table->string('currency_code')->nullable();
            $table->decimal('total_price', 9,2)->nullable();
            $table->integer('number')->nullable();
            $table->string('commission_kind')->nullable();
            $table->string('commission_rate')->nullable();
            $table->string('commission_value')->nullable();
            $table->dateTime('purchase_date')->nullable();
            $table->dateTime('pay_date')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('deleted_at')->nullable();

            $table->foreign('campaign_id')->references('campaign_id')->on('campaigns');
            $table->foreign('customer_id')->references('customer_id')->on('customers');
            $table->foreign('customer_media_id')->references('customer_media_id')->on('customer_media');
            $table->foreign('coupon_id')->references('coupon_id')->on('coupons');
            $table->foreign('coupon_history_id')->references('coupon_history_id')->on('coupon_histories');
            $table->engine = 'InnoDB';
        });

        Schema::create('currencies', function(Blueprint $table) {
            $table->increments('currency_id');
            $table->string('code')->nullable();
            $table->string('sign')->nullable();
            $table->string('rate')->nullable();
            $table->engine = 'InnoDB';
        });

        Schema::create('countries', function(Blueprint $table) {
            $table->increments('country_id');
            $table->string('region_code', 3)->nullable();
            $table->string('country_iso2', 2)->nullable();
            $table->string('country_iso3', 3)->nullable();
            $table->string('country_name_en')->nullable();
            $table->string('country_name_jp')->nullable();
            $table->string('country_name_zh-cn')->nullable();
            $table->string('country_name_zh-tw')->nullable();
            $table->engine = 'InnoDB';
        });
        Schema::create('user_admin_level', function(Blueprint $table) {
            $table->increments('admin_id');
            $table->string('level', 16)->nullable();
            $table->string('name')->nullable();
            $table->engine = 'InnoDB';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
