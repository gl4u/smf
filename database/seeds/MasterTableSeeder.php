<?php

use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'code' => 'HKD',
            'sign' => 'HK$',
        ];
        \DB::table('currencies')->insert($data);

        $country_data = [
            'region_code' => 'ASI',
            'country_iso2' => 'CN',
            'country_iso3' => 'CHN',
            'country_name_en' => 'China',
            'country_name_jp' => '中国',
            'country_name_zh-cn' => '中国',
            'country_name_zh-tw' => '中國',
        ];
        \DB::table('countries')->insert($country_data);

        $country_data2 = [
            'region_code' => 'ASI',
            'country_iso2' => 'HK',
            'country_iso3' => 'HKG',
            'country_name_en' => 'Hong Kong',
            'country_name_jp' => '香港',
            'country_name_zh-cn' => '香港',
            'country_name_zh-tw' => '香港',
        ];
        \DB::table('countries')->insert($country_data2);

        $admin_data = [
            'level' => 'admin',
            'name' => 'Administrator',
        ];
        \DB::table('user_admin_level')->insert($admin_data);

        $admin_data = [
            'level' => 'general',
            'name' => 'General User',
        ];
        \DB::table('user_admin_level')->insert($admin_data);

    }
}
