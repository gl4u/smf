<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use \Carbon\Carbon as Carbon;
use MikeMcLin\WpPassword\Facades\WpPassword;

class ClientController extends BaseController
{
    public function getIndex(){
        $select_result = \DB::connection('affilix')->table('clients')->get();
        //ユーザーデータの移行
        \DB::beginTransaction();
        try {

            foreach($select_result as $val){
                //users
                $user_data = [
                    'status' => 'valid',
                    'admin_level' => 'general',
                    'created_at' => $val->created_at,
                ];
                $lastInsertedID = \DB::table('users')->insertGetId($user_data);

                //user_auth
                $user_email_auth_data = [
                    'user_id' => $lastInsertedID,
                    'user_email' => 'ida@menard.com.hk',
                    'user_password' => WpPassword::make('client_password'),
                    'verified' => 1,
                    'created_at' => $val->created_at,
                ];
                \DB::table('user_email_auths')->insert($user_email_auth_data);

                $client_data = [
                    'client_id' => $val->id,
                    'user_id' => $lastInsertedID,
                    'client_code' => $val->client_code,
                    'client_name' => $val->client_name,
                    'created_at' => $val->created_at,
                ];
                $lastInsertedClientID = \DB::table('clients')->insertGetId($client_data);

                $select_campaigns = \DB::connection('affilix')->table('campaigns')
                    ->where('clients_id', $val->id)
                    ->get();

                foreach($select_campaigns as $val2){
                    $campaign_data = [
                        'campaign_id' => $val2->id,
                        'client_id' => $lastInsertedClientID,
                        'campaign_code' => $val2->campaign_code,
                        'campaign_name' => $val2->campaign_name,
//                        'short_description' => $val2->user_nicename,
                        'description' => $val2->description,
                        'start_date' => $val2->start_date,
                        'end_date' => $val2->end_date,
                        'currency_code' => $val2->currency_code,
                        'reward_kind' => $val2->kind,
                        'reward_rate' => $val2->rate,
                        'redirect_url' => $val2->redirect_url,
                        'image_name' => $val2->image_name,
                        'status' => $val2->status,
                        'created_at' => Carbon::now(),
                    ];
                    $lastInsertedCampaignID = \DB::table('campaigns')->insertGetId($campaign_data);

                    $select_coupons = \DB::connection('affilix')->table('coupons')
                        ->where('campaigns_id', $val2->id)
                        ->get();

                    foreach($select_coupons as $val3){

                        $coupon_data = [
                            'coupon_id' => $val3->id,
                            'campaign_id' => $val3->campaigns_id,
                            'coupon_name' => $val3->coupons_name,
                            'publish_date' => $val3->publish_date,
                            'expire_date' => $val3->expire_date,
                            'kind' => $val3->kind,
//                            'distribution_number' => $val3->currency_code,
                            'currency_code' => $val3->kind,
                            'discount_value' => $val3->discount_value,
                            'description' => $val3->description,
                            'image' => $val3->img_name,
                            'valid_period_date' => $val3->valid_period_date,
                            'valid_period_hours' => $val3->valid_period_hours,
                            'status' => $val3->status,
                            'created_at' => $val3->publish_date,
                        ];
                        $lastInsertedCouponID = \DB::table('coupons')->insertGetId($coupon_data);

                    }

                }
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            print_r($e);
            exit;
        }
        print_r('OK');
        exit;

        $select_result = WpPassword::make('SWSmMCCL');

        $password = 'SWSmMCCL';
        $wp_hashed_password = '$P$BAIpu9jo7TD0oBljPM2ZxiqG0tAsvz1';

        if ( WpPassword::check($password, $wp_hashed_password) ) {
            echo 1;

            // Password success!
        } else {
            echo 2;

            // Password failed :(
        }


        print_r($select_result);
        exit;
    }
}
