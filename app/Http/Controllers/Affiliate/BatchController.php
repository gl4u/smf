<?php

namespace App\Http\Controllers\Affiliate;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class BatchController extends BaseController
{
    public function getIndex()
    {
        $now_date = \Carbon\Carbon::now();

        $facebook = new Facebook(Config::get('facebook'));
        $access_token = $facebook->getAccessToken();
        //Get Checkin info
        //▼----------------
        //Get Customer info from Affilix
        $data = null;
        $sql  = '  SELECT';
        $sql .= '  customers.id';
        $sql .= ', customers.facebook_id';
        $sql .= '  FROM';
        $sql .= '  customers';
        $sql .= "  WHERE    customers.facebook_id   IS NOT NULL";
        $sql .= "  AND      customers.facebook_id   != ''";

        $selectResult = \DB::select($sql);

        //Get FB info from Affilix
        $idx = 0;
        foreach($selectResult as $fid) {
            if(!empty($fid)){
                $url = 'https://graph.facebook.com/' . $fid->facebook_id . '/feed?access_token=' . $access_token;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $json_data = curl_exec($ch);
                $all_data = json_decode($json_data);
                foreach ($all_data as $val) {
                    foreach($val as $val2) {
                        if(!empty($val2->place) && !empty($val2->picture)){
                            $result = \DB::table('fb_checkins')->where('fb_checkin_id', $val2->id)->first();
                            if(empty($result)){
                                $result = DB::table('fb_checkins')->insertGetId(
                                    array(
                                        'customers_id'    => $fid->id,
                                        'fb_checkin_id'   => $val2->id,
                                        'fb_page_id'      => $val2->place->id,
                                    )
                                );
                            }
                        }
                        $idx++;
                    }
                }
            }
        }
        //▲----------------

        /*
        ▼-----------------
        //Analyze smile

        $i = 0;
        //Get smile level
        $facepp = new \Libraries\Facepp();
        foreach($data as $val) {
            $params=array(
                'url'=>$val["picture_url"]
            ,'attribute'=>'glass,pose,gender,age,race,smiling'
            );
            $response = $facepp->execute('/detection/detect',$params);
            //Insert smile level
            if($response['http_code']==200){
                #json decode
                $data[$i] = json_decode($response['body'],1);
                if(!empty($data[$i]['face'])) {
                    for ($m = 0; $m < count($data[$i]['face']); $m++) {
                        //Client ID & campaign_uid
                        $sql = '  SELECT';
                        $sql .= '  HEX(client_spot.client_uid) as client_uid';
                        $sql .= ', HEX(client_campaign.campaign_uid) as campaign_uid';
                        $sql .= ', client_campaign.rate';
                        $sql .= '  FROM';
                        $sql .= '  client_spot';
                        $sql .= '  INNER JOIN client_campaign ON client_campaign.client_uid = client_spot.client_uid';
                        $sql .= '  where fb_spot_id = ?';
                        $arrayVal = array(
                            $val['spot_id']
                        );
                        $tmpData = \DB::select($sql, $arrayVal);

                        //Point Culc
                        $point = $tmpData[0]->rate * $data[$i]['face'][$m]['attribute']['smiling']['value'];
                        $point_hist_uid = \Libraries\UniqueId::generate();
                        $slq = '  INSERT INTO point_hist(';
                        $slq .= '  point_hist_uid';
                        $slq .= ', campaign_uid';
                        $slq .= ', point';
                        $slq .= ', client_uid';
                        $slq .= ', user_uid';
                        $slq .= ', created_at';
                        $slq .= ' )VALUES(';
                        $slq .= ' UNHEX(?), UNHEX(?), ?, UNHEX(?), UNHEX(?), ?)';
                        $arrayVal = array(
                            $point_hist_uid
                        , $tmpData[0]->campaign_uid
                        , $point
                        , $tmpData[0]->client_uid
                        , $val['user_uid']
                        , $now_date
                        );

                        \DB::beginTransaction();
                        try {
                            \DB::insert($slq, $arrayVal);
                            \DB::commit();
                        } catch (\Exception $e) {
                            \DB::rollback();
                            return 'NG';
                        }
                    }
                }else{
                    print("●error => ");
                    print_r($params);
                    print_r($val["picture_url"]);
                }
            }
            $i++;
        }
        ▲----------------
        */
        exit;
    }
}
