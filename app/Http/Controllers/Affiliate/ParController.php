<?php

namespace App\Http\Controllers\Affiliate;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class TrafficController extends BaseController
{
    public function getIndex()
    {
        //Insert Customer info
        $inputs = \Input::get();

        empty($inputs['c']) ? $campaigns_code = null : $campaigns_code = $inputs['c'];
        empty($inputs['m']) ? $media_code = null : $media_code = $inputs['m'];

        //Get affiliater info
        $affiliate_info = \DB::table('affiliaters')
            ->join('affiliaters_media', 'affiliaters.id', '=', 'affiliaters_media.affiliaters_id')
            ->where('affiliaters_media_code', $media_code)
            ->where('affiliaters.status', \Config::get('status.valid'))
            ->where('affiliaters.deleted_at', null)
            ->first();
        //Get campaign info
        $campaign_info = \DB::table('campaigns')
            ->where('status', \Config::get('status.valid'))
            ->where('campaign_code', $campaigns_code)
            //->where('start_date', '<=', \Carbon\Carbon::now())
            //->where('end_date', '>', \Carbon\Carbon::now())
            ->where('deleted_at', null)
            ->orderBy('created_at', 'desc')
            ->first();

        if(!empty($affiliate_info) && !empty($campaign_info)){
            //Cookie remain time default 3週間
            $interval = \Config::get('period.cookie_keep');
            if (empty($_COOKIE["customers_code"])) {
                //Make New Customer info
                $last_customer_code = \DB::table('customers')->max('id');
                $new_number = $last_customer_code + 1;
                $customers_code = \Config::get('definition.prefix_customer_code').$new_number;
                \DB::table('customers')->insert(
                    array(
                        'customers_code' => $customers_code,
                        'init_media_code' => $media_code,
                        'country' => 'HK',
                        'created_at' => \Carbon\Carbon::now()
                    )
                );
                setcookie("customers_code", $customers_code, time() + 60 * 60 * 24 * 365, '/');
            }
            //Set Cookie
            setcookie('media_code', $media_code, time() + $interval, '/');
            setcookie('campaigns_code', $campaigns_code, time() + $interval, '/');
        }

        //Move site
        return \Redirect::to($campaign_info->redirect_url);
    }

    public function getComplete()
    {
        //img tag
        /*
        https://www.affilix.tv/p/complete?
        number=5
        total_price=4500
         */
        //Set Paranetar
        $inputs = \Input::get();

        empty($_COOKIE["campaigns_code"]) ? $campaigns_code = null : $campaigns_code = $_COOKIE["campaigns_code"];
        empty($_COOKIE["media_code"])     ? $media_code = null : $media_code = $_COOKIE["media_code"];
        empty($_COOKIE["customers_code"]) ? $customers_code = null : $customers_code = $_COOKIE["customers_code"];

        $number         = 0;
        $total_price    = 0;
        $affiliater_id  = null;

        if(!empty($inputs['number']))       $number         = $inputs['number'];
        if(!empty($inputs['total_price']))  $total_price    = $inputs['total_price'];
        if(!empty($inputs['order_id']))     $order_id       = $inputs['order_id'];

        //Get affiliater info
        $affiliate_info = \DB::table('affiliaters')
            ->join('affiliaters_media', 'affiliaters.id', '=', 'affiliaters_media.affiliaters_id')
            ->where('affiliaters_media_code', $media_code)
            ->where('affiliaters.status', \Config::get('status.valid'))
            ->where('affiliaters.deleted_at', null)
            ->first();

        //Get campaign info
        $campaign_info = \DB::table('campaigns')
            ->where('status', \Config::get('status.valid'))
            ->where('campaign_code', $campaigns_code)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>', \Carbon\Carbon::now())
            ->where('deleted_at', null)
            ->orderBy('created_at', 'desc')
            ->first();
        //Customer info
        $customers_info = \DB::table('customers')
            ->where('customers_code', $customers_code)
            ->first();

        if(!empty($campaign_info) and !empty($affiliate_info)){

            $affiliaters_id = $affiliate_info->affiliaters_id;
            $affiliaters_media_id = $affiliate_info->id;
            $commission_kind = $campaign_info->kind;
            $commission_rate = $campaign_info->rate;

            empty($affiliaters_id)          ? $affiliaters_id = null   : $affiliaters_id = $affiliaters_id;
            empty($affiliaters_media_id)    ? $affiliaters_media_id = null        : $affiliaters_media_id = $affiliaters_media_id;
            empty($commission_kind)         ? $commission_kind = null        : $commission_kind = $commission_kind;
            empty($commission_rate)         ? $commission_rate = 0           : $commission_rate = $commission_rate;

            //Fix
            if($commission_kind == 'rate'){
                $pay_value = $total_price * $commission_rate;
            }else{
                //Variable
                $pay_value = $number * $commission_rate;
            }

            //Insert Data
            $result = \DB::table('purchases')->insert(
                array(
                    'affiliaters_id'        => $affiliaters_id,
                    'customers_id'           => $customers_info->id,
                    'affiliaters_media_id'  => $affiliaters_media_id,
                    'campaigns_id'          => $campaign_info->id,
                    'order_id'              => $order_id,
                    'total_price'           => $total_price,
                    'number'                => $number,
                    'commission_kind'       => $commission_kind,
                    'commission_rate'       => $commission_rate,
                    'commission_value'      => $pay_value,
                    'status'                => \Config::get('status.valid'),
                    'purchase_date'         => \Carbon\Carbon::now(),
                    'created_at'         => \Carbon\Carbon::now(),
                )
            );
        }

        $my_img = \imagecreate( 1, 1 );
        $background = \imagecolorallocate( $my_img, 255, 255, 255 );
        header( "Content-type: image/png" );
        imagepng( $my_img );
        exit;
    }
}
