<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use \Carbon\Carbon as Carbon;
use MikeMcLin\WpPassword\Facades\WpPassword;

class WorkingController extends BaseController
{
    public function getIndex(){
        $select_result = \DB::connection('affilix')->table('wp_users')->get();

        //ユーザーデータの移行
        \DB::beginTransaction();
        try {

            foreach($select_result as $val){
                $select_affiliater = \DB::connection('affilix')->table('affiliaters')
                    ->leftJoin('affiliaters_media', 'affiliaters.id', '=', 'affiliaters_media.affiliaters_id')
                    ->where('affiliaters.wp_users_id', $val->ID)
                    ->get();

                //users
                $user_data = [
                    'user_id' => $val->ID,
                    'status' => 'valid',
                    'admin_level' => 'general',
                    'created_at' => $val->user_registered,
                ];
                $lastInsertedID = \DB::table('users')->insertGetId($user_data);

                //user_auth
                $user_email_auth_data = [
                    'user_id' => $lastInsertedID,
                    'user_email' => $val->user_email,
                    'user_password' => $val->user_pass,
                    'verified' => 1,
                    'created_at' => $val->user_registered,
                ];
                \DB::table('user_email_auths')->insert($user_email_auth_data);

                $customer_data = [
                    'user_id' => $lastInsertedID,
                    'email' => $val->user_email,
                    'display_name' => $val->user_nicename,
                ];
                $lastInsertedCustomerID = \DB::table('customers')->insertGetId($customer_data);

                foreach($select_affiliater as $val2){
                    $customer_media_data = [
                        'customer_id' => $lastInsertedCustomerID,
                        'media_theme' => 'cosmetic',
                        'customer_media_code' => $val2->affiliaters_media_code,
                        'media_kind' => $val2->media_kind,
                        'name' => $val2->name,
                        'url' => $val2->url,
                    ];
                    \DB::table('customer_media')->insert($customer_media_data);
                }

            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            print_r($e);
            exit;
        }
        print_r('OK');
        exit;

        $select_result = WpPassword::make('SWSmMCCL');

        $password = 'SWSmMCCL';
        $wp_hashed_password = '$P$BAIpu9jo7TD0oBljPM2ZxiqG0tAsvz1';

        if ( WpPassword::check($password, $wp_hashed_password) ) {
            echo 1;

            // Password success!
        } else {
            echo 2;

            // Password failed :(
        }


        print_r($select_result);
        exit;
    }
}
