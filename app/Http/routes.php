<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controller('traffic', 'Affiliate\TrafficController');
Route::controller('p','Affiliate\ParController');
Route::controller('batch','Affiliate\BatchController');


Route::controller('customer','CustomerController');


Route::controller('client', 'ClientController');
Route::controller('working', 'WorkingController');
/*
Route::controller('/p','ParController');
Route::controller('/test','TestController');
Route::controller('/form','FormController');
Route::controller('/batch','BatchController');
Route::controller('/customer','CustomerController');
Route::controller('/client','ClientController');


Route::get('/login/fb/callback','IndexController@register');
Route::get('/login/fb', function() {
    $facebook = new Facebook(Config::get('facebook'));
    $params = array(
        'redirect_uri' => url('/login/fb/callback'),
        'scope' => 'email',
    );
    return Redirect::to($facebook->getLoginUrl($params));
});

Route::post('/index', array('before' => 'csrf', function(){
    $inputs = Input::only(array('username', 'password'));
    if ( Auth::attempt($inputs) ) {
        Route::get('/index/finish','IndexController@finish');
    } else {
        Route::get('/index/register','IndexController@register');
    }
}));
Route::controller('/index','IndexController');


Route::controller('/','IndexController');

*/